require 'test_helper'

class SubcategoryTest < ActiveSupport::TestCase
  test 'valid subcategory' do
    sub = Subcategory.new(name: 'Kasmir', category_id: 1)
    assert sub.valid?
  end

  test 'invalid without name' do
    sub = Subcategory.new(category_id: 1)
    refute sub.valid?
    assert_not_nil sub.errors[:name]
  end

  test 'invalid without category_id' do
    sub = Subcategory.new(name: "Kasmir")
    refute sub.valid?
    assert_not_nil sub.errors[:category_id]
  end
end
