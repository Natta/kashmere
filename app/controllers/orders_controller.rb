class OrdersController < ApplicationController
  before_action :admin_only, except: :user_orders
  
  def index
    @filter = params[:filter]
    @sort = params[:sort]
    @search = params[:search]
        
    @dropdown_filter = dropdown_filter
    @dropdown_sort = dropdown_sort
    
    @orders = Order.filter(@filter)
    @orders = Order.sort(@orders, @sort)
    
    if @search
      @orders = Order.search(@orders, @search)
    end
    
    @page = params[:page]
    @orders = @orders.paginate(page: @page, per_page: 10)
  end
    
  def user_orders
    if current_user
      @orders = Order.get_user_orders(current_user.email)
    end
  end
  
  def update_order
    @order = Order.find(params[:id])
    status = params[:status]
    if @order.update(:status => status)
      redirect_to orders_path(sort: params[:sort], 
                                    filter: params[:filter], 
                                    page: params[:page])
    end
  end
    
  def destroy
    @order.destroy
    orders_path
  end
    
  private
  
  def dropdown_filter
    if @filter.nil? or @filter == ''
      'Filtriraj po statusu'
    elsif @filter == 'all'
      'Svi statusi'
    else
      @filter
    end
  end
    
  def dropdown_sort
    if @sort.nil? or @sort == ''
      'Sortiraj po datumu'
    elsif @sort.include? 'latest'
      'Najskoriji'
    else
      'Najstariji'
    end
  end
    
end
