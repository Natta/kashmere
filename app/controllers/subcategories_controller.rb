class SubcategoriesController < ApplicationController
  before_action :admin_only
  before_action :set_subcategory, only: [:show, :edit, :update, :destroy]

  def index
    @subcategories = Subcategory.all
    @page = params[:page]
    @subcategories = @subcategories.paginate(page: @page, per_page: 15)
  end

  def new
    @subcategory = Subcategory.new
    @categories = Category.all
  end

  def edit
    @categories = Category.all
  end

  def create
    @subcategory = Subcategory.new(subcategory_params)
    if @subcategory.save
      redirect_to subcategories_url
    else
      redirect_to new_subcategory_path
    end
  end

  def update
    if @subcategory.update(subcategory_params)
      redirect_to subcategories_url
    else
      redirect_to edit_subcategory_path
    end
  end

  def destroy
    @subcategory.destroy
    redirect_to subcategories_url
  end

  private

    def set_subcategory
      @subcategory = Subcategory.find(params[:id])
    end

    def subcategory_params
      params.require(:subcategory).permit(:name, :category_id)
    end
end
