class CheckoutController < ApplicationController
  before_action :find_cart
  before_action :set_no_cache, except: :success
  after_action :delete_cart, only: :success
  protect_from_forgery with: :null_session
    
  def index
    @delivery = params[:delivery]
    @flag = 'checkout'
    if current_user
      redirect_to :action => 'order', :delivery => @delivery
    end
    
    @step_text = step_text
  end
    
  def order
    @step_text = step_text
    @user = ""
    @company_data = ""
    @delivery = params[:delivery]
    if params[:id]
      id = Hashids.new("kashmeresaltforkashmere", 10).
        decode(params[:id]).
        try(:first)
      @user = User.find(id)
      @company_data = @user.company_data
    elsif current_user
      @user = current_user
      @company_data = @user.company_data
    end
  end
    
  def success
    @order = Order.last()
    @step_text = step_text
  end
    
  def place_order
    order = Order.new(order_params)
    order.customer_ip = request.remote_ip
    cart = Cart.find(params[:order][:cart])
    populate_order(cart, order)
    if Order.count > 0
      id = Order.last.id + 1
    else
      id = 1
    end
    number = order.generate_order_number(id)
    order.order_number = number
    session[:order_number] = number
        
    if order.save
      cart.cart_items.destroy_all
      redirect_to checkout_success_path
      OrderMailer.order_confirmation(order).deliver
    else
      flash[:notice] = "Error while placing order.'#{order.error_message}'"
      render :action => 'index'
    end
  end
    
    
  def check_cart
    if @cart.number_of_items == 0
      redirect_to root_path
    end
  end
  private
  
    def populate_order(cart, order)
      for cart_item in cart.cart_items
        order_item = OrderItem.new(:product_id => cart_item.product_id,
                                   :price => cart_item.price,
                                   :amount => cart_item.amount,
                                   :color => cart_item.color)
        order.order_items << order_item
      end
    end
        
    def order_params
      params.require(:order).permit(:company, :delivery, :ship_to_company_name, 
                                    :pib, :crn, :id, :email, :customer_ip, 
                                    :ship_to_first_name, :ship_to_last_name, 
                                    :phone_number, :ship_to_address, 
                                    :ship_to_city, :ship_to_postal_code, 
                                    :error_message, :updated_at, :created_at)
    end
    
end
