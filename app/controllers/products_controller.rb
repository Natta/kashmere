class ProductsController < ApplicationController
  before_action :admin_only
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  
  # GET /products
  # GET /products.json
  def index
    @categories = Category.get_all
    if params[:search]
      @products = Product.search(params[:search])
      @dropdown_filter = 'Filtriraj po kategoriji'
    elsif params[:sub]
      @products = Product.filter(false, params[:sub])
      @dropdown_filter = Subcategory.find_name(params[:sub])
    elsif params[:cat]
      @products = Product.filter(true, params[:cat])
      @dropdown_filter = Category.find_name(params[:cat])
    elsif params[:all]
      @products = Product.get_all
      @dropdown_filter = 'Sve'
    else
      @products = Product.get_all
      @dropdown_filter = 'Filtriraj po kategoriji'
    end
    @products = @products.paginate(page: params[:page], per_page: 12)
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
    @subcategories = Subcategory.all
  end

  # GET /products/1/edit
  def edit
    @subcategories = Subcategory.all
    @page = params[:page]
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    if params[:images]
      if @product.save
        create_images
        flash[:success] = "Proizvod je napravljen."
        redirect_to products_url
      else
        redirect_to new_product_path
      end
    else
      flash[:danger] = "Proizvod mora imati minimum jednu sliku."
      redirect_to new_product_path
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    if @product.update(product_params)
      edit_images_availability
      create_images
      if destroy_images
        redirect_to products_path(page: params[:page])
      else
        redirect_to edit_product_path
      end
    else
      redirect_to edit_product_path
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    if Order.order_processed?(@product.id)
      @product.destroy
    else
      flash[:notice] = 'Proizvod se ne može obrisati zbog statusa narudžbine!'
    end
      redirect_to products_url
  end

  private
  
  def create_images
    if params[:images]
      params[:images].each { |image| @product.attachments.create(image: image)}
    end
  end
  
  def edit_images_availability
    available_images = params[:available]
    if available_images
      available_images.each do |id, value|
        image = @product.get_attachment(id)
        image.available = value
        image.save!
      end
    end
  end
  
  def destroy_images
    destroy_images = params[:destroy]
    if destroy_images
      if destroy_images.values.count('1') == @product.attachments.length
        flash[:notice] = 'Mora postojati minimum jedna fotografija'
        return false
      else
        destroy_images.each do |id, value|
          image = @product.get_attachment(id)
          if value == '1'
            image.destroy
          end
        end
        return true
      end
    end
  end
    
  def set_product
    @product = Product.find(params[:id])
  end
  
  def product_params
    params.require(:product).permit(:name, :price, :description, 
                                    :subcategory_id, :sku, :piece, images:[], 
                                    delete_images:[], available_images:[])
  end
end
