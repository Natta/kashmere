class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :find_cart
  helper_method :sub_per_column
  
  private
  
  def after_sign_in_path_for(resource)
    if params[:flag] == 'checkout'
      checkout_order_url(current_user) #your path
    else
      root_path
    end
  end
  
  def after_sign_out_path_for(resource)
    home_registration_path
  end
  
  def find_cart
    if session[:cart_id]
      @cart = Cart.find_by_id(session[:cart_id])
      if @cart.nil?
        session[:cart_id] = nil
      end
    end
  end
      
  def delete_cart
    if session[:cart_id]
      @cart = Cart.find(session[:cart_id])
      @cart.destroy
      session[:cart_id] = nil
    end
  end
  
  def set_no_cache
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end
  
  def step_text
    if current_user
      'Vaši podaci'
    else
      'Registracija'
    end
  end
  
  def admin_only
    if !current_user || !current_user.admin?
      redirect_to root_path 
    end
  end
  
  def sub_per_column(category_id)
    all_subs = Subcategory.get_number_of_subcategories_by_category(category_id)
    all_subs = all_subs / 3
    if all_subs == 0
      all_subs = 1
    end
    all_subs
  end
end
