class HomeController < ApplicationController

    def registration
        @option = params[:option]
    end
    
    def contact
    end
    
    def send_mail
        email = params[:email]
        subject = params[:subject]
        body = params[:comments]
        ContactMailer.contact_mailer(email, subject, body).deliver
        redirect_to contact_path, notice: 'Poruka je uspešno poslata. Hvala što ste nas kontaktirali!'
    end
    
end
