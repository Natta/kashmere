class CartController < ApplicationController
  before_action :initialize_cart
    
  def index
    if @cart.get_total_items == 0
      redirect_to root_path
    end
    @button_text = 'Registracija'
    if current_user
      @button_text = 'Nastavi'
    end
    @step_text = step_text
  end
    
  def add
      @product = Product.get_product(params[:product][:id])
      amount = params[:amount]
      color = params[:color]
      if @product.check_attachment_availability(color) and amount.to_f.between?(0.5, 999)
        @item = @cart.add(@product.id, color, amount)
        flash[:success] = %Q[Proizvod je dodat u korpu.]
      else
        if amount.to_f <= 0 or amount.to_f > 999
          flash[:danger] = "Količina koju ste uneli ne može se dodati u korpu."
        else
          flash[:danger] = "Ovaj proizvod ne možete dodati u korpu."
        end
      end
      redirect_to catalog_show_path(id: @product.id)
  end
    
  def add_amount
    if params[:item] and params[:amount]
      @item = CartItem.find(params[:item])
      @item.amount = params[:amount]
      @item.save!
    end
    respond_to do |format|  
      format.js 
    end  
  end
    
  def remove
    @product = Product.find(params[:id])
    @item = @cart.remove(params[:id], params[:color])
    redirect_to cart_path
  end
    
  def initialize_cart
    if session[:cart_id]
      @cart = Cart.find(session[:cart_id])
    else
      @cart = Cart.create
      session[:cart_id] = @cart.id
    end
  end
  
end