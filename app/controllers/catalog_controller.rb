class CatalogController < ApplicationController
  before_action :find_cart
  
  def index
    filter_by_category
      
    @search_term = params[:q]  
    if @search_term
      @products = @products.search(@search_term)
      @category_title = 'Svi proizvodi'
    end
      
    if params[:sort] && params[:sort] == 'desc'
      @products = @products.order('price DESC')
    else
      @products = @products.order('price ASC')
    end
    @products = @products.paginate(page: params[:page], per_page: 12)
  end 

  def show
    @product = Product.find(params[:id])
  end
  
  def filter_by_category
    if params[:sub]
      @products = Product.filter(false, params[:sub])
      @category_title = Subcategory.find_name(params[:sub])
    elsif params[:cat]
      @products = Product.filter(true, params[:cat])
      @category_title = Category.find_name(params[:cat])
    else
      @products = Product.get_all
      @category_title = 'Svi proizvodi'
    end
  end
  
  def from_category
    filter_by_category
    if params[:sort] && params[:sort] == 'desc'
      @products = @products.order( 'price DESC' )
    else
      @products = @products.order( 'price ASC' )
    end
    respond_to do |format|
      format.js
    end
  end

end
