/* global $*/
function check_empty(element) {
    if(element.value == '') {
        element.style.borderBottom = '1px solid #ff4848';
        return true;
    }
    else
    return false;
}

function validate_form(validation_class) {
    var valid = true;
    if ($('#user_company_true').is(':checked')) {
        
        var fields = document.getElementsByClassName('company-data');

        for (var i = 0; i < fields.length; i++)
            if (check_empty(fields[i]))
                valid = false;
    }
    var user_fields = document.getElementsByClassName(validation_class);
    for (var i = 0; i < user_fields.length; i++) 
            if (check_empty(user_fields[i]))
                valid = false;
                
    return valid;
}
function validate_number(number) {
    if(number > 0 && number < 1000)
        return true;
    else
        return false;
}