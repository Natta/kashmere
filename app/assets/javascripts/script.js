/*global $*/
/* global jQuery*/
$(document).ready(function() {
    
    /*$("#btn-register").click(function(event) {
        check_empty($('#user_email'));
        if (!validate_form()) 
            return false;
        });*/
    
   
    
    $("#btn-forgot-password").click(function(event) {
        if(!validate_form("validation-password"))
            return false;
    });
    
    $("#btn-submit").click(function(event) {
        if(!validate_form("validation-product")) 
            return false;
    });
    
    $("#btn-order").click(function(event) {
        if(!validate_form("validation-order"))
        return false;
    });
        
    $("#add_to_cart_submit").click(function(event) {
       if(!validate_number($('#amount').val()))
        return false;
    });
    
    $(".form-control").keyup(function() {
        if($(this).value != '')
            $(this).css('border-bottom', "1px solid #d8d8d8");
    });
    
    $(".form-control").focusin(function() {
        if($(this).value == '')
            $(this).css('border-bottom', "1px solid #397ebb");
    });
    
//error home -> registration/login
    if($('#option').val() == 'register') {
        $('#register-div').removeClass('hide-at-first');
        $('#login-div').addClass('hide-at-first');
        $('#login-btn').addClass('cool-link');
        $('#login-btn').removeClass('cool-link-active');
        $('#register-btn').addClass('cool-link-active');
        $('#register-btn').removeClass('cool-link');
    }
    
    status_color();
 //CATALOG HOVER TRIGGER
    /* orders/index status modal, izabrani status */
    
    $("div[id^='stat-btn']").click(function() {
       var id = $(this).attr('id').substr(8);
       var modal_class = ".modal-status" + id;
       var text = $(this).text().trim();
       $(modal_class + '> .glyphicon-ok').remove();
       var span = document.createElement("span");
       span.className = 'glyphicon glyphicon-ok';
       $(modal_class + ':contains(' + text + '):first').append(span);
    });
    
    $(".sub-item:first").addClass('t-line');
    if ($("#sort").text().indexOf("Sortiraj") == -1)
        $("#sort").addClass('link-c b');

    if ($("#filter-status").text().indexOf("Filtriraj") == -1)
        $("#filter-status").addClass('link-c b');

    var available_images = [];
    $("input[id^='available']").on('change', function() {
        var check_value = false;
        if ($(this).is(":checked"))
            check_value = true;
        var id = $(this).attr('id').substr(10);
        var index = $.inArray(id, available_images);
        if (index == -1) {
            available_images.push(id);
            available_images.push(check_value);
        }
        else {
            available_images[index + 1] = check_value;
        }
        $("#available_images").val(available_images);
    });

    var destroy_images = [];
    $("input[id^='destroy']").on('change', function() {
        var check_value = false;
        if ($(this).is(":checked"))
            check_value = true;
        var id = $(this).attr('id').substr(8);
        var index = $.inArray(id, destroy_images);
        if (index == -1) {
            destroy_images.push(id);
            destroy_images.push(check_value);
        }
        else {
            destroy_images[index + 1] = check_value;
        }
        $("#destroy_images").val(destroy_images);
    });
    
    $("#add_to_cart_submit").click(function() {
        $("#add_to_cart_form").submit();
    });
    
    /* total, catalog/show */
    $("#amount").on('change', function() {
        if ($(this).attr('step') == '1')
        $(this).val(Math.round($(this).val()));
        var price = $("#price").text();
        price = price.replace(" RSD", '')
                     .replace('.', '')
                     .replace(',', '.');
        var total = price * $(this).val();
        
        if(total % 1 != 0)
            $("#total").text(total.toLocaleString('de-DE', { minimumFractionDigits: 2 }) + " RSD");
        else 
            $("#total").text(total.toLocaleString('de-DE', { minimumFractionDigits: 0 }) + " RSD");
    });
    
    $("#amount").keypress(function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).blur();
        }
    });

    $('.tft').on('change', function() {
        if ($(this).attr('step') == '1')
        $(this).val(Math.round($(this).val()));
        var id = $(this).attr("id");
        $.ajax({
            url: "cart/add_amount",
            type: "POST",
            data: {
                item: id,
                amount: $("#" + id).val()
            }
        });
    });

    $('.dropdown-menu-a').on('click', function() {
        var linktext = $(this).text();
        $('#filter').text(linktext).css({'color':'#397ebb','font-weight':'bold'});
        $('.dropdown-menu').hide();
    });
    
    $('.dropdown-toggle').on('click', function() {
        if ($(this).hasClass('status-toggle')) {
            if ($('#menu-status').is(':visible'))
                $('#menu-status').hide();
            else
                $('#menu-status').show();
        }
        else {
            if ($('#menu').is(':visible'))
                $('#menu').hide();
            else
                $('#menu').show();
        }
    });

    function closeMenu() {
        $('.dropdown-menu').hide();
    }

    $(document.body).click(function(e) {
        closeMenu();
    });

    $(".dropdown-toggle").click(function(e) {
        e.stopPropagation(); 
    });


    $("#btn-order").prop('disabled','true'); //onload
    /*CART FORMS SHOW/HIDEs*/
    $("#login-btn").click(function() {
        $("#login-btn").addClass('cool-link-active').removeClass('cool-link');
        $("#register-btn").removeClass('cool-link-active').addClass('cool-link');
        $("#guest-btn").removeClass('cool-link-active').addClass('cool-link');
        $("#register-div").hide();
        $("#guest-div").hide();
        $("#forgot-pass-div").hide();
        $("#login-div").fadeIn(300);
        $("#btn-order").prop('disabled','true');
    });

    $("#register-btn").click(function() {
        $("#register-btn").addClass('cool-link-active').removeClass('cool-link');
        $("#login-btn").removeClass('cool-link-active').addClass('cool-link');
        $("#guest-btn").removeClass('cool-link-active').addClass('cool-link');
        $("#login-div").hide();
        $("#forgot-pass-div").hide();
        $("#guest-div").hide();
        $("#register-div").fadeIn(300);
        $("#btn-order").prop('disabled','true');
    });

    $("#guest-btn").click(function() {
        $("#guest-btn").addClass('cool-link-active').removeClass('cool-link');
        $("#register-btn").removeClass('cool-link-active').addClass('cool-link');
        $("#login-btn").removeClass('cool-link-active').addClass('cool-link');
        $("#login-div").hide();
        $("#forgot-pass-div").hide();
        $("#register-div").hide();
        $("#guest-div").fadeIn(300);
        $("#btn-order").removeAttr('disabled');
    });

    $("#request-new-pass").click(function() {
        $("#login-div").fadeOut(300, function() {
            $("#forgot-pass-div").fadeIn(300);
        });
    });
    
    $("#return-to-log").click(function() {
        $("#forgot-pass-div").fadeOut(300, function() {
            $("#login-div").fadeIn(300);
        });
    });
    
    company_selected();
    
    $("#user_company_true").click(function() {
        $("#company").slideDown(150);
    });

    $("#user_company_false").click(function() {
        $("#company").slideUp(300);
    });

    $("#order_company_true").click(function() {
        $("#guest-company").slideDown(150);
    });

    $("#order_company_false").click(function() {
        $("#guest-company").slideUp(300);
    });

    $("#delivery_true").click(function() {
        $("#store-div2").fadeOut(500, function() {
            $("#post-div2").fadeIn(500);
        });
        $('#tot-price').addClass('col-md-6 col-sm-6 col-xs-6').removeClass('col-md-12 col-sm-12 col-xs-12');
        setTimeout(fade, 550);
    });

    $("#delivery_false").click(function() {
        $("#post-div2").fadeOut(500, function() {
            $("#store-div2").fadeIn(500);
        });
        $("#post-div1").fadeOut(500, function() {
            $('#tot-price').addClass('col-md-12 col-sm-12 col-xs-12').removeClass('col-md-6 col-sm-6 col-xs-6');
        });
    });

    if ($("#delivery_false").is(':checked')) {
        $("#post-div2").fadeOut(500, function() {
            $("#store-div2").fadeIn(500);
        });
        $("#post-div1").fadeOut(500, function() {
            $('#tot-price').addClass('col-md-12 col-sm-12 col-xs-12').removeClass('col-md-6 col-sm-6 col-xs-6');
        });
    }

    // CLEARABLE INPUT
    $(document).on('input', '.clearable', function() {
        $(this)[tog(this.value)]('x');
    }).on('mousemove', '.x', function(e) {
        $(this)[tog(this.offsetWidth - 18 < e.clientX - this.getBoundingClientRect().left)]('onX');
    }).on('touchstart click', '.onX', function(ev) {
        ev.preventDefault();
        $(this).removeClass('x onX').val('').change();
    });

    // ADMIN LIST PAGE DROPDOWN
    $(".status").click(function() {
        var linktext = $(this).text();
        var filtertext = $("#filter-status").text();
        $("#filter-status").text(linktext).css("color", "#397ebb").addClass("b");
    });
    
    //BUTTON SPINNER
    $(".spin").parent().click(function(){
        var text = $(this).attr('id').substring(4);
        if(!validate_form("validation-" + text)) 
            return false;
        else
            $('span.glyphicon-repeat', this).removeClass('hide-at-first');
    });

    //HOVER STATUS BUTTON
    $('div[id^=stat-btn]').hover(function() {
        var id = $(this).attr('id').substr(8);
        $('#pen-glyph' + id).delay(100).css('display', 'inline').slide(100);
        
    },
    function() {
        var id = $(this).attr('id').substr(8);
        $('#pen-glyph' + id).hide(100).slide(100);
    });

    //slider options
    $('#lightSlider').lightSlider({
        gallery: true,
        item: 1,
        loop: false,
        slideMove: 1,
        slideMargin: 0,
        enableDrag: true,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 800,
        thumbItem: 8,
        responsive: [{
            breakpoint: 800,
            settings: {
                item: 1,
                slideMove: 1,
                slideMargin: 6,
            }
        }, {
            breakpoint: 480,
            settings: {
                item: 1,
                slideMove: 1
            }
        }]
    });
    
    $('.lightSlider').each(function() {
        $(this).lightSlider({
            gallery: true,
            item: 1,
            loop: false,
            slideMove: 1,
            slideMargin: 0,
            enableDrag: true,
            easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
            speed: 800,
            thumbItem: 8,
            responsive: [{
                breakpoint: 800,
                settings: {
                    item: 1,
                    slideMove: 1,
                    slideMargin: 6,
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            }]
        });
    });

    //STOCK IMG BUTTONS
    $(".stock").click(function() {
        if ($(this).hasClass("stock-inactive"))
            $(this).removeClass("stock-inactive").addClass("stock-active");
        else
            $(this).addClass("stock-inactive").removeClass("stock-active");
    });

    $(".remove-img").click(function() {
        if ($(this).hasClass("remove-img-active")) {
            $(this).siblings('img').animate({
                'opacity': '1'
            }, 'fast');
            $(this).removeClass('remove-img-active');
        }
        else {
            $(this).siblings('img').animate({
                'opacity': '0.15'
            }, 'fast');
            $(this).addClass('remove-img-active');
        }
    });
    //CART-TRIGGER
    $('#cart-trigger').click(function(){
        $('.search-box').hide();
        $('.cart-widget').fadeToggle(200);
    });
    //cart-dismiss
    $('#cart-widget-dismiss').click(function(){
        $('.cart-widget').fadeOut(200);
    });
    // USER-SEARCH
    //focus
    $('.user-search').focusin(function(){
       $('.search-box').css('border','1px solid #397ebb'); 
    });
    $('.user-search').focusout(function(){
       $('.search-box').css('border','1px solid #a7a7a7'); 
    });
    //trigger
    if($('.user-search').val() != '')
        $('.search-box').show();
        
    $('#search-trigger').click(function(){
       $('.cart-widget').hide();
       $('.search-box').fadeToggle(200);
       $('.user-search').focus();
    });
    
    $('.search-box').focusout(function(){
       $('.search-box').fadeOut(200);
    }); 
       
    //CATALOG-DROP
    $("li[id^='cat-']").hover(function() {
       var id = $(this).attr('id').substr(4);
        $('#menu-' + id).finish().toggle();
    });
    
    $('#close-catalog').click(function(){
        $('.catalog-menu').finish().fadeOut('normal');
    });
    
    //Catalog-item-hover
    $('.item-card').hover(function(){
        $(this).find('.cat-card-details').toggleClass('card-details-btn');
    });
    
   //FILEUPLOAD NUMBER OF FILES
   $(".upload_images").on("change", function(){   
        var numFiles = $(this)[0].files.length;
        $('#number_of_images').text('Pripremljenih slika: ' + numFiles);
    });

    //TOOLTIPS
    $('.stock').tooltip({
        title: "Stanje na lageru",
        placement: "right",
        delay: {
            show: 500,
            hide: 100
        }
    });
    
    //NAVBAR invert on hover 
    $('.navbar').hover(function () {
        remove_if_root($(this));
        
    },
    function() {
        add_if_root($(this));
       
    });
    $('.cart-widget').mouseleave(function () {
        if($('#root').text() === 'true')
            $('.cart-widget').fadeOut(200);
    });
    if($('#root').text() === 'true')
        $('.navbar').css('border-bottom','none');
    
    // NAVBAR invert on first scroll
    window.onscroll = function() {
        if ( window.pageYOffset > 60 ) 
        {
            remove_if_root($('.navbar'));
            $('.cart-widget').fadeOut(200);
        } 
        else
        {
            add_if_root($('.navbar'));
        }
    }
    
    // SIDE MENU FOR MOBILE
    $('#menu-icon').click(function(){
        $('.side-menu-shade').fadeIn(200);
        $('#side-menu').addClass('side-menu-opened');
        $('.navbar').addClass('page-push-left');
        $('.content-wrap').addClass('page-push-left');
    });
    $('.side-menu-shade').add('.close-menu').click(function(){
        $('.side-menu-shade').fadeOut(200);
        $('#side-menu').removeClass('side-menu-opened');
        $('.navbar').removeClass('page-push-left');
        $('.content-wrap').removeClass('page-push-left');
    });
    // Side-menu slide down subcats
    $("div[id^='m-cat-']").click(function() {
       var opt_id = $(this).attr('id').substr(6);
        $('#m-menu-' + opt_id).finish().toggle(350);
    });
    
    // LABELS SHOW ON TYPE
    
        
        
});

function status_color() {
    $(".os-label:contains('U pripremi')").addClass('os-label-orange');
    $(".os-label:contains('Poslato')").addClass('os-label-blue');
    $(".os-label:contains('Plaćeno')").addClass('os-label-green');
    $(".os-label:contains('Vraćeno')").addClass('os-label-red');
}
function company_selected() {
    if ($("#user_company_true").is(':checked'))
    $("#company").show();
} 
function fade() {
    $("#post-div1").fadeIn("normal");
}
function tog(v) {
    return v ? 'addClass' : 'removeClass';
}
jQuery.fn.exists = function exists(){ return this.length > 0; }
function check(input) {
    if (input.val().indexOf('@') == -1) {
     input.get(0).setCustomValidity('Email mora imati @');
   } else  {
     input.get(0).setCustomValidity('');
   }
}

function remove_if_root(element) {
    if($('#root').text() === 'true')
        {
            element.removeClass('transparent-nav');
            $('.navbar-brand').removeClass('invert-navbar-brand');
            $('.logo2').removeClass('invert-logo2');
            
        }
}

function add_if_root(element) {
    if($('#root').text() === 'true')
        {
            element.addClass('transparent-nav');
            $('.navbar-brand').addClass('invert-navbar-brand');
            $('.logo2').addClass('invert-logo2');
        }
}