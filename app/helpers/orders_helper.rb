module OrdersHelper
  def link_to_orders(type, value)
    orders_path(params.merge(type => value).except(:controller, :action))
  end
end
