module ApplicationHelper
    def resource_name
        :user
    end

    def resource
        @resource ||= User.new
    end

    def devise_mapping
        @devise_mapping ||= Devise.mappings[:user]
    end
    
    def set_home_class(new_class)
        if current_page?(root_url)
            new_class
        else
            ""
        end
    end
    
    def get_username
        if current_user.first_name
            return current_user.first_name
        end
        return current_user.email
    end
end
