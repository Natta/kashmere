module CatalogHelper
  def sort(value)
    fetch_products_path(params.merge(sort: value).except(:controller, :action))
  end
  
  def no_products
    if @products.count() == 0
      return true
    end
    return false
  end
  
end
