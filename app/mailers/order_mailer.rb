class OrderMailer < ApplicationMailer
    default from: 'metraza-kasmir@gmail.com'

  def order_confirmation(order)
    @order = order
    mail(to: @order.email, subject: 'Narudžbina je primljena')
  end
end
