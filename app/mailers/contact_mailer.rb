class ContactMailer < ApplicationMailer
    default to: 'micho.angelos@gmail.com'

    def contact_mailer(email, subject, body)
        @email = email
        @subject = subject
        @body = body

        mail(from: email, subject: subject)
    end
end