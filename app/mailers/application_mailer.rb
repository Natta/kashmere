class ApplicationMailer < ActionMailer::Base
  default from: "metraza-kasmir@gmail.com"
  layout 'mailer'
end
