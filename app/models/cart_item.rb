class CartItem < ActiveRecord::Base
  include Itemable
  belongs_to :cart
  belongs_to :product
  validates :amount, numericality: {greater_than_or_equal_to: 0, less_than: 999}
  
end
