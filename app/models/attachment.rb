class Attachment < ActiveRecord::Base
  has_attached_file :image, :restricted_characters => /[&$+,\/:;=?@<>\[\]\{\}\|\\\^~%#]/,
    styles: {thumb: "100x100#", :large => "500x500#" }
  validates_attachment_content_type :image, 
    :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
  belongs_to :product
  
  def get_file_name
    image_file_name.split('.')[0]
  end
  
  def get_file_name_underscore
    get_file_name().tr(' ', '_')
  end
  
  def get_image_file_name
    image_file_name.chomp('.jpg').chomp('.JPG').chomp('.png').chomp('.jpeg')
  end
end
