class Category < ActiveRecord::Base
  has_many :subcategories
  
  def self.find_name(id)
      Category.where(:id => id).first.name
  end
  
  def self.get_all
    Category.all.includes(:subcategories).order(:id)
  end
end
