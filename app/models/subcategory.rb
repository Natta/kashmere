class Subcategory < ActiveRecord::Base
  has_many :products
  belongs_to :category
  validates :name, presence: true
  validates :category_id, presence: true
  
  def self.find_name(id)
    Subcategory.where(:id => id).first.name
  end
  
  def self.get_number_of_subcategories_by_category(category_id)
    Subcategory.where(:category_id => category_id).all.count
  end
  
  def get_category_name
    category.name
  end
end
