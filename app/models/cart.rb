class Cart < ActiveRecord::Base
  has_many :cart_items, dependent: :destroy
  has_many :products, :through => :cart_items
    
  def get_total_price
    cart_items.inject(0) {|sum, item| item.price * item.amount + sum}
  end
    
  #def get_total_quantity
   # cart_items.inject(0) {|sum, item| item.amount + sum}
  #end
  
  def get_total_items
    cart_items.all.count
  end
    
  def add(product_id, color, amount)
    items = cart_items.where('product_id = ? and color = ?', 
                             product_id, color)
    product = Product.find(product_id)
      if items.size < 1
        cart_item = cart_items.create(:product_id => product_id,
                               :amount => amount,
                               :price => product.price,
                               :color => color)
      else
        cart_item = items.first
        cart_item.update_attribute(:amount, cart_item.amount + amount.to_i)
      end
      cart_item
  end
    
  def remove(product_id, color)
    cart_item = cart_items.where('product_id = ? and color = ?', product_id, color).first
    CartItem.destroy(cart_item)
    cart_item
  end

end
