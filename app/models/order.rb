class Order < ActiveRecord::Base 
  has_many :order_items, dependent: :destroy
  has_many :products, :through => :order_items
    #dodati validaciju!!!
    
  def self.get_user_orders(username)
    Order.where(:email => username).includes(order_items: :product)
  end
  
  def self.sort(orders, type)
    if type == 'oldest'
      orders.order(created_at: :asc)
    else
      orders.order(created_at: :desc)
    end
  end
    
  def self.filter(type)
    if type.nil? or type == 'all' or type == ''
      Order.all.includes(order_items: :product)
    else
      Order.where(:status => type).includes(order_items: :product)
    end
  end
  
  def self.order_processed?(product_id)
    order_items_by_product = OrderItem.where(product_id: product_id)
    order_items_by_product.each do |item|
      order = Order.where(id: item.order_id).first
      if order && (order.status == 'U pripremi' || order.status == 'Poslato')
        return false
      end
    end
    return true
  end
  
  def self.search(orders, keyword)
    keywords = keyword.split(' ')
    if keywords.length > 1
       orders.where("lower(ship_to_first_name) LIKE ? and 
                 lower(ship_to_last_name) LIKE ?", 
                 "%#{keywords[0].downcase}%", "%#{keywords[1].downcase}%")
                 .includes(order_items: :product)
    else
      orders.where("lower(ship_to_first_name) LIKE ? OR 
                   lower(ship_to_last_name) LIKE ? OR 
                   order_number = ? OR 
                   lower(ship_to_company_name) LIKE ? 
                   OR email LIKE ?", 
                   "%#{keyword.downcase}%", "%#{keyword.downcase}%", 
                   "#{keyword}", "%#{keyword.downcase}%", 
                   "%#{keyword}%").includes(order_items: :product)
    end
  end
    
  def delivery_type
    if delivery
      "Kurirska služba"
    else
      "Samostalno"
    end
  end
    
  def get_total_price
    order_items.inject(0) {|sum, item| item.price * item.amount + sum}
  end
    
  def generate_order_number(id)
    Hashids.new("ordernumbersalt", 7).encode(id)
  end
  
end
