class Product < ActiveRecord::Base
  has_many :attachments, dependent: :destroy
  belongs_to :subcategory
  has_many :cart_items
  has_many :carts, :through => :cart_items
  validates :name, :price, :sku, :subcategory, presence: {
  message: 'mora' }
  
  def get_measure_amount
    if piece
      "kom"
    else
      "m"
    end
  end
    
  def get_measure_type
    if piece
      "po komadu"
    else
      "po metru"
    end
  end
    
  def get_category_name
    if subcategory
      subcategory.get_category_name
    end
  end
    
  def get_subcategory_name
    if subcategory
      subcategory.name
    end
  end
    
  def show_image(color='none')
    if color == 'none'
      attachments.first.image(:large)
    else
      attachments.where('image_file_name LIKE ?', "%#{color}%").first.image(:large)
    end
  end
  
  def get_attachment(id)
    attachments.where("id = ?", id).first
  end
  
  def check_attachment_availability(color)
    attachments.where("replace(image_file_name, ' ', '_') LIKE replace(?, ' ', '_')", "%#{color}%").first.available
  end
    
  private

  def self.filter(category, val)
    if category
      subcategories = Subcategory.where('category_id = ?', val)
      sub_ids = []
      subcategories.each {|sub| sub_ids.push(sub.id)}
      return Product.where(:subcategory_id => sub_ids).
        includes(:attachments).includes(subcategory: [:category])
    else
      return Product.where("subcategory_id = ?", val).
        includes(:attachments).includes(subcategory: [:category])
    end
  end
  
  def self.search(keyword)
    keyword = I18n.transliterate(keyword).downcase 
    Product.where("translate(lower(name), 'čćšđžČĆŠĐŽ', 'ccsdzccsdz') LIKE ?", 
      "%#{keyword}%")
  .includes(:attachments).includes(subcategory: [:category])
  end
  
  def self.get_all
    Product.all.includes(:attachments).includes(subcategory: [:category])
  end
  
  def self.get_product(id)
    Product.find(id)
  end
  
end
