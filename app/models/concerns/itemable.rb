module Itemable
  extend ActiveSupport::Concern
  
  def chosen_image
    product.show_image(color)
  end

  def get_total_price
    amount * price
  end
  
end