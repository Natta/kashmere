class User < ActiveRecord::Base
  has_one :company_data
  validates :phone_number, :first_name, :last_name, :address, :city, :postal_code, presence: {
  message: 'mora' }
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
         
  accepts_nested_attributes_for :company_data
  
  def get_full_name
    if !admin
      first_name + ' ' + last_name
    end
  end
end
