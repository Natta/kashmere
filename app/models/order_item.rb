class OrderItem < ActiveRecord::Base
  include Itemable
  belongs_to :order
  belongs_to :product
  validates :amount, numericality: {greater_than_or_equal_to: 0, less_than: 999}
    
  def product_available?
    if Product.find_by(id: product_id)
      return true
    else
      return false
    end
  end
    
  def get_product_name
    if product_available?
      product.name
    else
      'Proizvod više ne postoji'
    end
  end
  
  def get_product_measure
    if product_available?
      product.get_measure_amount
    else
      ''
    end
  end
  
end
