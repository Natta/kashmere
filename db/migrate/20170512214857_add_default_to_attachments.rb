class AddDefaultToAttachments < ActiveRecord::Migration
  def change
    change_column :attachments, :availability, :boolean, :default => true
  end
end
