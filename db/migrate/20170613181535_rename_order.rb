class RenameOrder < ActiveRecord::Migration
  def change
    rename_column :orders, :is_company, :company
    rename_column :orders, :is_delivery, :delivery
  end
end
