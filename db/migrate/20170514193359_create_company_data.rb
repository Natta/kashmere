class CreateCompanyData < ActiveRecord::Migration
  def change
    create_table :company_data do |t|
      t.string :company_name
      t.string :pib
      t.string :crn
      t.timestamps null: false
    end
    add_column :user_data, :company, :boolean, :default => false
  end
end
