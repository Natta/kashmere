class AddDefault < ActiveRecord::Migration
  def change
    change_column :orders, :status, :string, :default => 'U pripremi'
  end
end
