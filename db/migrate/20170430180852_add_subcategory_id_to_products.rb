class AddSubcategoryIdToProducts < ActiveRecord::Migration
  def change
    add_column :products, :subcategory_id, :integer
    add_column :subcategories, :category_id, :integer
  end
end
