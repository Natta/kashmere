class RemoveEmailFromUserData < ActiveRecord::Migration
  def change
    remove_column :user_data, :email
  end
end
