class CreateUserData < ActiveRecord::Migration
  def change
    create_table :user_data do |t|
      t.string :email
      t.string :phone_number
      
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :postal_code
      
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
