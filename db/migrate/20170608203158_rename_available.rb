class RenameAvailable < ActiveRecord::Migration
  def change
    rename_column :attachments, :availability, :available
  end
end
