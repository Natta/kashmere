class IntToFloat < ActiveRecord::Migration
  def change
    change_column :cart_items, :amount, :float
    change_column :order_items, :amount, :float
  end
end
