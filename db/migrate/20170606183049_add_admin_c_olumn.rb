class AddAdminCOlumn < ActiveRecord::Migration
  def change
    add_column :users, :admin, :boolean, :default => false
    remove_column :users, :role_id
    drop_table :roles
  end
end
