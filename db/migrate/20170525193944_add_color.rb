class AddColor < ActiveRecord::Migration
  def change
    add_column :cart_items, :color, :string
    add_column :order_items, :color, :string
  end
end
