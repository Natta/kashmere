class RemoveColFromUserData < ActiveRecord::Migration
  def change
    remove_column :user_data, :company
    add_column :users, :is_company, :boolean, :default => false
    add_column :company_data, :user_id, :integer
  end
end
