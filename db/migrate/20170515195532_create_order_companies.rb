class CreateOrderCompanies < ActiveRecord::Migration
  def change
    create_table :order_companies do |t|
      t.string :ship_to_company_name
      t.string :pib
      t.string :crn
      t.integer :order_id
      t.timestamps null: false
    end
    add_column :orders, :is_company, :boolean, :default => false
  end
end
