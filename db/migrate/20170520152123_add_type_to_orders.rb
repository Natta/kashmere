class AddTypeToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :is_delivery, :boolean
  end
end
