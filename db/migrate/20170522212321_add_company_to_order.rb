class AddCompanyToOrder < ActiveRecord::Migration
  def change
    drop_table :order_companies
    add_column :orders, :ship_to_company_name, :string
    add_column :orders, :crn, :string
    add_column :orders, :pib, :string
  end
end
