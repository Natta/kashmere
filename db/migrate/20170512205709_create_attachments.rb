class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.boolean :availability
      t.timestamps null: false
    end
    
    add_attachment :attachments, :image
    remove_column :products, :available
    add_column :products, :piece, :boolean
    remove_attachment :products, :image
  end
end
