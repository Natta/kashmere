class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price
      t.string :description
      t.boolean :available
      t.attachment :image

      t.timestamps null: false
    end
  end
end
