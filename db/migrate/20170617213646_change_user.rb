class ChangeUser < ActiveRecord::Migration
  def change
    rename_column :users, :is_company, :company
  end
end
