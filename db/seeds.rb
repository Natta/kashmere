# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Category.create(name: 'Modni tekstil')
Category.create(name: 'Posteljni tekstil')
Category.create(name: 'Pozamanterija')

# Subcategory.create(name: 'Postava', category_id: 1)
# Subcategory.create(name: 'Puplin', category_id: 1)
# Subcategory.create(name: 'Likra', category_id: 1)
# Subcategory.create(name: 'Dekor', category_id: 1)
# Subcategory.create(name: 'Lepljivo platno', category_id: 1)
# Subcategory.create(name: 'Džepovina', category_id: 1)
# Subcategory.create(name: 'Angin', category_id: 1)
# Subcategory.create(name: 'Pliš', category_id: 1)
# Subcategory.create(name: 'Čipka', category_id: 1)
# Subcategory.create(name: 'Svila', category_id: 1)
# Subcategory.create(name: 'Viskoza', category_id: 1)
# Subcategory.create(name: 'Muslin', category_id: 1)
# Subcategory.create(name: 'Saten', category_id: 1)
# Subcategory.create(name: 'Lan', category_id: 1)
# Subcategory.create(name: 'Keper', category_id: 1)
# Subcategory.create(name: 'Poliester', category_id: 1)
# Subcategory.create(name: 'Štof', category_id: 1)
# Subcategory.create(name: 'Somot', category_id: 1)
# Subcategory.create(name: 'Krzno', category_id: 1)
# Subcategory.create(name: 'Skuba i punto milano', category_id: 1)
# Subcategory.create(name: 'Vuna', category_id: 1)

# Subcategory.create(name: 'Krpe', category_id: 2)
# Subcategory.create(name: 'Kanafas', category_id: 2)
# Subcategory.create(name: 'Stoljnjaci', category_id: 2)
# Subcategory.create(name: 'Posteljina', category_id: 2)
# Subcategory.create(name: 'Pamučni saten', category_id: 2)
# Subcategory.create(name: 'Damast', category_id: 2)
# Subcategory.create(name: 'Krep', category_id: 2)

# Subcategory.create(name: 'Konci', category_id: 3)
# Subcategory.create(name: 'Rajsferšlusi', category_id: 3)

# Role.create(name: 'admin')
# Role.create(name: 'regular')

