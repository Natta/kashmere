Rails.application.routes.draw do

  root 'catalog#index'
  
  get 'zohoverify/verifyforzoho' => 'zohoverify#verifyforzoho', :defaults => { :format => "html" }
  get 'home/registration' => 'home#registration'
  get 'contact' => 'home#contact'
  match '/send_mail', to: 'home#send_mail', via: 'post'
  
  devise_for :users, :controllers => { registrations: 'users/registrations', 
                                       sessions: 'users/sessions',
                                       confirmations: 'users/confirmations',
                                       passwords: 'users/passwords'
  }
  
  post 'products/new'
  get "/fetch_products" => 'catalog#from_category', as: 'fetch_products'
  get 'catalog/show'

  get 'catalog/search'

  get 'cart' => 'cart#index', as: 'cart'
  
  post 'checkout' => 'checkout#index'
  
  get 'checkout/order' => 'checkout#order'
  
  post 'checkout/place_order'
  post 'cart/add_amount'
  get 'cart/add_amount'
  post 'products/select_category'
  post 'products/select_category'
  get 'checkout/success' => 'checkout#success'
  
  get 'orders' => 'orders#index'
  post 'orders' => 'orders#index'
  get 'my_orders' => 'orders#user_orders'
  
  post 'subcategories/create' => 'subcategories_create_path'
  
  post 'cart/add' => 'cart#add'
  post 'cart/add/:id' => 'cart#add'
  post 'cart/remove/:id' => 'cart#remove'
  post 'cart' => 'cart#index'
  post 'cart/add_amount' => 'cart#add_amount'
  resources :subcategories
  resources :products
  
  post '/update_order/:id' => "orders#update_order", as: :update_order
  
  
  #devise_scope :user do
 # get "sign_up", to: "users/registrations#new"
#end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # 

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     #   end
end
