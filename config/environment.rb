# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!
ActionMailer::Base.smtp_settings = {
:address => 'smtp.sendgrid.net',
:port => '587',
:authentication => :plain,
:user_name => ENV['SENDGRID_USERNAME'],
:password => ENV['SENDGRID_PASSWORD'],
:domain => 'mail.metraza-kasmir.rs',
:enable_starttls_auto => true
}

unless Rails.env.production?
  Paperclip::Attachment.default_options.merge!({
    :url => "/system/:rails_env/:class/:attachment/:id_partition/:style/:filename",
    :path => ":rails_root/public:url"
  })
end
